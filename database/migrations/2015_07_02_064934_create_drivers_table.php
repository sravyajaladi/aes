<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('driver_name');
            $table->double('mobile_no')->unsigned();
            $table->string('blood_group');
            $table->string('transporter_number');
            $table->string('emergency_details');
            $table->integer('driver_rating')->unsigned();
            $table->string('driver_license_number');
            $table->string('driver_license_location');
            $table->date('driver_license_issue_date');
            $table->date('driver_license_renewal_date');
            $table->string('passport_number');
            $table->string('passport_size_photo');
            $table->string('passport_location');
            $table->date('passport_issue_date');
            $table->string('passport_authorizers');
            $table->date('passport_jde_expiry_date');
            $table->string('passport_expiry_criteria');
            $table->integer('finger_print_id')->unsigned();
            $table->string('status');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('drivers');
    }
}
