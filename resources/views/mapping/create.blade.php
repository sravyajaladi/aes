@extends('layouts.admin')

@section('pagelevelstyles')
<!-- BEGIN PAGE LEVEL STYLES -->
{!! HTML::style('/assets/admin/global/plugins/select2/select2.css') !!}
{!! HTML::style('/assets/admin/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css') !!}
{!! HTML::style('/assets/admin/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css') !!}
{!! HTML::style('/assets/admin/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') !!}
{!! HTML::style('/assets/admin/global/plugins/star-rating/lib/jquery.raty.css') !!}
{!! HTML::style('/assets/admin/global/plugins/select2/select2.css') !!}
{!! HTML::style('/assets/admin/global/plugins/nifty-modal/css/component.css') !!}
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME STYLES -->
{!! HTML::style('/assets/admin/global/css/components-md.css') !!}
{!! HTML::style('/assets/admin/global/css/plugins-md.css') !!}
{!! HTML::style('/assets/admin/admin/layout/css/layout.css') !!}
{!! HTML::style('/assets/admin/admin/layout/css/themes/darkblue.css') !!}
{!! HTML::style('/assets/admin/admin/layout/css/custom.css') !!}
<!-- END THEME STYLES -->
@stop


@section('maincontent')
	<!-- BEGIN CONTENT -->
	<button style="visibility:hidden" class="md-trigger" data-modal="modal-14"></button>

	<div class="md-modal md-effect-14" id="modal-14">
		<div class="md-content">
			<h3>Alert</h3>
			<div class="dialog-msg"></div>
			<div>
				<button type="button" class="btn btn-primary blue md-close">
					<span class="md-click-circle md-click-animate"></span>Close
				</button>
			</div>
		</div>
	</div>

	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			Launch Trip <small>form...</small>
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Trip</a>
					</li>
				</ul>
				<div class="page-toolbar">
					<div class="btn-group pull-right">
						<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
						Actions <i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu pull-right" role="menu">
							<li>
								<a href="#">Action</a>
							</li>
							<li>
								<a href="#">Another action</a>
							</li>
							<li>
								<a href="#">Something else here</a>
							</li>
							<li class="divider">
							</li>
							<li>
								<a href="#">Separated link</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<div class="portlet box blue" id="form_wizard_1">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-truck"></i> Trip Wizard - <span class="step-title">
								Step 1 of 4 </span>
							</div>
							<div class="tools hidden-xs">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form action="/mapping" class="form-horizontal" id="submit_form" method="POST">
								<div class="form-wizard">
									<div class="form-body">
										<ul class="nav nav-pills nav-justified steps">
											<li>
												<a href="#tab1" data-toggle="tab" class="step">
												<span class="number">
												1 </span>
												<span class="desc">
												<i class="fa fa-check"></i> Select Trip </span>
												</a>
											</li>
											<li>
												<a href="#tab2" data-toggle="tab" class="step">
												<span class="number">
												2 </span>
												<span class="desc">
												<i class="fa fa-check"></i> Login Driver </span>
												</a>
											</li>
											<li>
												<a href="#tab3" data-toggle="tab" class="step active">
												<span class="number">
												3 </span>
												<span class="desc">
												<i class="fa fa-check"></i> Login Assistant </span>
												</a>
											</li>
											<li>
												<a href="#tab4" data-toggle="tab" class="step">
												<span class="number">
												4 </span>
												<span class="desc">
												<i class="fa fa-check"></i> Select Truck </span>
												</a>
											</li>
										</ul>
										<div id="bar" class="progress progress-striped" role="progressbar">
											<div class="progress-bar progress-bar-success">
											</div>
										</div>
										<div class="tab-content">
											<!-- <div class="alert alert-danger display-none">
												<button class="close" data-dismiss="alert"></button>
												You have some form errors. Please check below.
											</div>
											<div class="alert alert-success display-none">
												<button class="close" data-dismiss="alert"></button>
												Your form validation is successful!
											</div> -->
											<div class="tab-pane active" id="tab1">
												<h3 class="block">Select Trip</h3>
												<div class="form-group">
													<label class="control-label col-md-3">Trip <span class="required">
													* </span>
													</label>
													<div class="col-md-4">
														{!! Form::select('trip_id', $trips, null, ['class' => 'select2_category form-control', 'data-placeholder' => 'Choose a Category', 'tabindex' => '1']) !!}
													</div>
												</div>
											</div>
											<div class="tab-pane" id="tab2">
												<h3 class="block">Login Driver</h3>
												<div class="form-group">
													<div class="control-label col-md-4">
													<button type="button" id="assign_driver" class="btn btn-primary green">
															<span class="md-click-circle md-click-animate"></span>Login Driver
														</button>
													</div>
													<p class="form-control-static">
														<input type="text" name="driver_name" data-required="1" class="form-control" readonly="" />
														<input type="hidden" name="driver_id">
													</p>
												</div>												
											</div>
											<div class="tab-pane" id="tab3">
												<h3 class="block">Login Assistant</h3>
												<div class="form-group">
													<div class="control-label col-md-4">
													<button type="button" id="assign_assistant" class="btn btn-primary green">
															<span class="md-click-circle md-click-animate"></span>Login Assistant
														</button>
													</div>
													<p class="form-control-static">
														<input type="text" name="assistant_name" data-required="1" class="form-control" readonly="" />
														<input type="hidden" name="assistant_id">
													</p>
												</div>
											</div>
											<div class="tab-pane" id="tab4">
												<h3 class="block">Select Truck</h3>
												<div class="form-group">
													<label class="control-label col-md-3">Truck</label>
													<div class="col-md-4">
														{!! Form::select('truck_id', $trucks, null, ['class' => 'select2_category form-control', 'data-placeholder' => 'Choose a Category', 'tabindex' => '1']) !!}
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="form-actions">
										<div class="row">
											<div class="col-md-offset-3 col-md-9">
												<a href="javascript:;" class="btn default button-previous">
												<i class="m-icon-swapleft"></i> Back </a>
												<a href="javascript:;" class="btn blue button-next">
												Continue <i class="m-icon-swapright m-icon-white"></i>
												</a>
												<button type="submit" class="btn green button-submit">
												Start <i class="m-icon-swapright m-icon-white"></i>
												</button>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->

	<div class="md-overlay"></div>
@stop

@section('pagelevelscripts')
<!-- BEGIN PAGE LEVEL PLUGINS -->
{!! HTML::script('/assets/admin/global/plugins/jquery-validation/js/jquery.validate.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/jquery-validation/js/additional-methods.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/select2/select2.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js') !!}
{!! HTML::script('/assets/admin/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js') !!}
{!! HTML::script('/assets/admin/global/plugins/ckeditor/ckeditor.js') !!}
{!! HTML::script('/assets/admin/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js') !!}
{!! HTML::script('/assets/admin/global/plugins/bootstrap-markdown/lib/markdown.js') !!}
{!! HTML::script('/assets/admin/global/plugins/star-rating/lib/jquery.raty.js') !!}
{!! HTML::script('/assets/admin/global/plugins/jquery-validation/js/additional-methods.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/select2/select2.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/nifty-modal/js/classie.js') !!}
{!! HTML::script('/assets/admin/global/plugins/nifty-modal/js/modalEffects.js') !!}
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
{!! HTML::script('/assets/admin/global/scripts/metronic.js') !!}
{!! HTML::script('/assets/admin/admin/layout/scripts/layout.js') !!}
{!! HTML::script('/assets/admin/admin/layout/scripts/quick-sidebar.js') !!}
{!! HTML::script('/assets/admin/admin/layout/scripts/demo.js') !!}
{!! HTML::script('/assets/admin/admin/pages/scripts/form-validation.js') !!}
{!! HTML::script('/assets/admin/admin/pages/scripts/form-samples.js') !!}
{!! HTML::script('/assets/admin/admin/pages/scripts/form-wizard.js') !!}
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {    
   	// initiate layout and plugins
   	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
	QuickSidebar.init(); // init quick sidebar
	Demo.init(); // init demo features
   	FormValidation.init();
   	FormSamples.init();
   	FormWizard.init();

   	$(".md-trigger").click(function(){
   		$( '.md-overlay' ).addClass("md-show");
   	});

   	$(".md-close").click(function(){
   		$( '.md-overlay' ).removeClass("md-show");
   	});

   	$("#assign_driver").click(function(){
	   	$(".fixed").css("display", "block");

   		$.ajax({
		  method: "GET",
		  url: "http://127.0.0.1:88/identify.php",
		  // async: false
		}).done(function( msg ) {
		    $("input[name='driver_id']").val(msg);

		    $.ajax({
			  method: "GET",
			  url: "/get/user/"+msg+"/driver",
			  // async: false
			}).done(function( msg ) {
			    if(msg.length == 0) {
					$(".md-trigger").click();
					$(".dialog-msg").html("Invalid User");
				    $(".fixed").css("display", "none");
				    return
				}

				if(msg.status == 0){
						$(".md-trigger").click();
						$(".dialog-msg").html("Driver is Blacklisted");
					    $(".fixed").css("display", "none");
					}else{
						$(".scan_status").css("display", "none");
						$("input[name='driver_name']").val(msg.driver_name);
					    $(".fixed").css("display", "none");
				}
			});
		});
   	});


   	$("#assign_assistant").click(function(){ 
   		$(".fixed").css("display", "block");

   		$.ajax({
		  method: "GET",
		  url: "http://127.0.0.1:88/identify.php",
		  // async: false
		}).done(function( msg ) {
		    $("input[name='assistant_id']").val(msg);
		    $.ajax({
			  method: "GET",
			  url: "/get/user/"+msg+"/assistant",
			  // async: false
			}).done(function( msg ) {
				
				if(msg.length == 0) {
					$(".md-trigger").click();
					$(".dialog-msg").html("Invalid User");
				    $(".fixed").css("display", "none");
				    return
				}

				if(msg.status == 0){
						$(".md-trigger").click();
						$(".dialog-msg").html("Assistant is Blacklisted");
					    $(".fixed").css("display", "none");
					}else{
						$(".scan_status").css("display", "none");
						$("input[name='assistant_name']").val(msg.assistant_name);
					    $(".fixed").css("display", "none");
				}

			  });
		  });
   	});

});
</script>
<!-- END JAVASCRIPTS -->
@stop