@extends('layouts.admin')

@section('pagelevelstyles')
<!-- BEGIN PAGE LEVEL STYLES -->
{!! HTML::style('/assets/admin/global/plugins/select2/select2.css') !!}
{!! HTML::style('/assets/admin/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css') !!}
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME STYLES -->
{!! HTML::style('/assets/admin/global/css/components-md.css') !!}
{!! HTML::style('/assets/admin/global/css/plugins-md.css') !!}
{!! HTML::style('/assets/admin/admin/layout/css/layout.css') !!}
{!! HTML::style('/assets/admin/admin/layout/css/themes/darkblue.css') !!}
{!! HTML::style('/assets/admin/admin/layout/css/custom.css') !!}
<!-- END THEME STYLES -->
@stop


@section('maincontent')
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">User Details</h4>
						</div>
						<div class="modal-body">
							
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			Locations <small>List of locations</small>
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Locations</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->

			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					@if(Session::has('message'))
					<div class="alert alert-success display-hide" style="display: block;">
						<button class="close" data-close="alert"></button>
						{{ Session::get('message') }}
					</div>
					@endif
					<div class="portlet box purple">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-user"></i>Manage Locations
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-toolbar">
								<div class="row">
									<div class="col-md-6">
										<div class="btn-group">
											<a href="/locations/create" id="sample_editable_1_new" class="btn purple">
											Add New <i class="fa fa-plus"></i>
											</a>
										</div>
									</div>
									<div class="col-md-6">
										<div class="btn-group pull-right">
											<button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
											</button>
											<ul class="dropdown-menu pull-right">
												<li>
													<a href="javascript:;">
													Print </a>
												</li>
												<li>
													<a href="javascript:;">
													Save as PDF </a>
												</li>
												<li>
													<a href="javascript:;">
													Export to Excel </a>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<table class="table table-striped table-bordered table-hover" id="sample_1">
							<thead>
							<tr>
								<th>
									 Name
								</th>
								<th>
									 Status
								</th>
								<th>
									Action
								</th>
							</tr>
							</thead>
							<tbody>
							<?php foreach($locations as $location){ ?>
								<tr class="odd gradeX">
									<td>
										 {{ $location->name }}
									</td>
									<td>
										@if($location->status == 1)
											<span class="label label-sm label-success"> Active </span>
										@else
											<span class="label label-sm label-info"> Inactive </span>
										@endif
									</td>
									<td>
										<a href="/locations/{{ $location->id }}" id="view" class="btn default btn-xs green-stripe"> View </a>		
										<a href="/locations/{{ $location->id }}/edit" id="edit" class="btn default btn-xs green-stripe"> Edit </a>		
									</td>
								</tr>
							<?php } ?>
							</tbody>
							</table>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>

		</div>
	</div>
	<!-- END CONTENT -->
@stop


@section('pagelevelscripts')
<!-- BEGIN PAGE LEVEL PLUGINS -->
{!! HTML::script('/assets/admin/global/plugins/select2/select2.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/datatables/media/js/jquery.dataTables.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') !!}
{!! HTML::script('/assets/admin/global/plugins/bootbox/bootbox.min.js') !!}
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
{!! HTML::script('/assets/admin/global/scripts/metronic.js') !!}
{!! HTML::script('/assets/admin/admin/layout/scripts/layout.js') !!}
{!! HTML::script('/assets/admin/admin/layout/scripts/quick-sidebar.js') !!}
{!! HTML::script('/assets/admin/admin/layout/scripts/demo.js') !!}
{!! HTML::script('/assets/admin/admin/pages/scripts/table-managed.js') !!}
{!! HTML::script('/assets/admin/admin/pages/scripts/ui-alert-dialog-api.js') !!}
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {    
   Metronic.init(); // init metronic core componets
   Layout.init(); // init layout
   QuickSidebar.init(); // init quick sidebar
   Demo.init(); // init demo features
   TableManaged.init();
   UIAlertDialogApi.init();
});
</script>
<!-- END JAVASCRIPTS -->
@stop
