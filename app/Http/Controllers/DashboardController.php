<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB, Response;
class DashboardController extends Controller
{
    public function showDashboard()
    {
    	return View('dashboard');
    }

    public function createUser($type)
    {
    	return DB::table('users')->insertGetId([
                            'name'       => 'jarvis',
                            'email'      => 'jarvis'.rand().'@meltag.com',
                            'type'       => $type,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s'),
                        ]);
    }

    public function getUser($userId, $type)
    {

        try {
    		$query = DB::table('users');

            if($type == 'driver')
                    $query->select('drivers.driver_name', 'drivers.status')
                            ->join('drivers', 'users.id', '=', 'drivers.user_id');
            else
                    $query->select('assistants.assistant_name', 'assistants.status')
                        	->join('assistants', 'users.id', '=', 'assistants.user_id');

    		$data = $query->where('users.id', $userId)->first();

            if(is_null($data))
                return Response::json([]);

            return Response::json($data);

        } catch (Exception $e) {
            return Response::json([]);   
        }
    }
}
