<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB, Input, Redirect;

class AssistantsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $assistants = DB::table('assistants')->get();
        return View('assistants.lists', compact('assistants'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View('assistants.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {

        $data = Input::all();
        $data['user_id']    = Input::get('user_id'); 
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['updated_at'] = date('Y-m-d H:i:s');

        DB::table('assistants')
                    ->insert($data);

        return Redirect::to('assistants')
                            ->with('message', 'Saved Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $assistant = DB::table('assistants')->where('id', $id)->first();
        return View('assistants.view', compact('assistant'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $assistant = DB::table('assistants')->where('id', $id)->first();
        return View('assistants.edit', compact('assistant'));        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $data               = Input::except(['_method', '_token']);
        $data['updated_at'] = date('Y-m-d H:i:s');

        DB::table('assistants')
                    ->where('id', $id)
                    ->update($data);

        return Redirect::to('assistants')
                            ->with('message', 'Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
