<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth, Redirect;

class LoginController extends Controller
{
    public function login()
    {
        return view('login');
    }

    public function loginUser(Request $request)
    {
        try {

            if ( ! Auth::attempt($request->only(['email', 'password']), $request->get('remember_me')))
                return Redirect::back()
                                ->with(['status' => false, 'message' => 'Please check your credentials']);

            // if($request->get('email') == 'admin@socialfeed.in')
            //     return Redirect::to('dashboard');
            // else
            //     return Redirect::to('feeds');

            return Redirect::to('dashboard');
        }
        catch (FormValidationException $e)
        {
            return Redirect::back()->withInput()
                                   ->withErrors($e->getErrors());
        }
    }

    public function logoutUser()
    {
        Auth::logout();
        return Redirect::to('/login');
    }
}
