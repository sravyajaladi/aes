<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB, Input, Redirect;

class DriversController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $drivers = DB::table('drivers')->orderBy('id', 'desc')->get();
        return View('drivers.lists', compact('drivers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View('drivers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $data                   = Input::except('score');
        $data['driver_rating']  = Input::get('score');
        $data['user_id']        = Input::get('user_id');
        $data['created_at']     = date('Y-m-d H:i:s');
        $data['updated_at']     = date('Y-m-d H:i:s');

        DB::table('drivers')
                    ->insert($data);

        return Redirect::to('drivers')
                            ->with('message', 'Saved Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $driver = DB::table('drivers')->where('id', $id)->first();
        return View('drivers.view', compact('driver'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $driver = DB::table('drivers')->where('id', $id)->first();
        return View('drivers.edit', compact('driver'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $data                   = Input::except(['score', '_method', '_token']);
        $data['driver_rating']  = Input::get('score');
        $data['updated_at']     = date('Y-m-d H:i:s');

        DB::table('drivers')
                    ->where('id', $id)
                    ->update($data);

        return Redirect::to('drivers')
                            ->with('message', 'Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
